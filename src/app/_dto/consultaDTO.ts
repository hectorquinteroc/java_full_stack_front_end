import { Venta } from './../_model/venta';
import { DetalleVentas } from './../_model/detalleVenta';

export class ConsultaDTO {
    venta: Venta;
    detalleVenta: DetalleVentas
}