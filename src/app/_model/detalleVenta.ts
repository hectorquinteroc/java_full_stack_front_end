import { Producto } from './producto';
import { Venta } from './venta';
export class DetalleVentas{
    idDetalleVenta: number;
    idVenta: Venta;
    idProducto: Producto;
    cantidad: number;
}