import { Persona } from './persona';
export class Venta {
    idFecha: number;
    fecha: string;
    idPersona: Persona;
    importe: number;
}