import { ConsultaDTO } from './../_dto/consultaDTO';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { GenericService } from './generic.service';
import { Venta } from './../_model/venta';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService extends GenericService<Venta>{

  constructor(protected http: HttpClient) { 
    super(
      http, `${environment.HOST}/consultas`
    );
    console.log(http);
  }

  registrarTransaccion(consultaDTO: ConsultaDTO){
    return this.http.post(this.url, consultaDTO);
  }
}
