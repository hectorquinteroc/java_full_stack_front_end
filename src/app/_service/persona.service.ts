import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { GenericService } from './generic.service';
import { Persona } from './../_model/persona';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonaService extends GenericService<Persona>{

  personaCambio = new Subject<Persona[]>();
  mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) { 
    super(
      http, `${environment.HOST}/personas`
    );
    console.log(http);
  }

  getPersonaCambio(){
    return this.personaCambio.asObservable();
  }

  setPersonaCambio(personas: Persona[]){
    this.personaCambio.next(personas);
  }

  getMensajeCambio(){
    return this.mensajeCambio.asObservable();
  }

  setMensajeCambio(mensaje: string){
    return this.mensajeCambio.next(mensaje);
  }
}
