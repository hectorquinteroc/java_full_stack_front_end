import { GenericService } from './generic.service';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Producto } from '../_model/producto';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductoService extends GenericService<Producto>{

  productoCambio = new Subject<Producto[]>();
  mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) { 
    super(
      http, `${environment.HOST}/productos`
    );
    console.log(http);
  }

  getProductoCambio(){
    return this.productoCambio.asObservable();
  }

  setProductoCambio(productos: Producto[]){
    this.productoCambio.next(productos);
  }

  getMensajeCambio(){
    return this.mensajeCambio.asObservable();
  }

  setMensajeCambio(mensaje: string){
    return this.mensajeCambio.next(mensaje);
  }
}
