import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Venta } from './../_model/venta';
import { GenericService } from './generic.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VentaService extends GenericService<Venta>{

  ventaCambio = new Subject<Venta[]>();
  mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) { 
    super(
      http, `${environment.HOST}/ventas`
    );
    console.log(http);
  }

  getProductoCambio(){
    return this.ventaCambio.asObservable();
  }

  setProductoCambio(ventas: Venta[]){
    this.ventaCambio.next(ventas);
  }

  getMensajeCambio(){
    return this.mensajeCambio.asObservable();
  }

  setMensajeCambio(mensaje: string){
    return this.mensajeCambio.next(mensaje);
  }
}
