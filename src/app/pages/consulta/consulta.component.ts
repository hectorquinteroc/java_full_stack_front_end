import { MatSnackBar } from '@angular/material/snack-bar';
import { ConsultaService } from './../../_service/consulta.service';
import { DetalleVentas } from './../../_model/detalleVenta';
import { ConsultaDTO } from './../../_dto/consultaDTO';
import { VentaService } from './../../_service/venta.service';
import { Venta } from './../../_model/venta';
import { ProductoService } from './../../_service/producto.service';
import { PersonaService } from './../../_service/persona.service';
import { Producto } from './../../_model/producto';
import { Persona } from './../../_model/persona';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  persona$: Observable<Persona[]>;
  producto$: Observable<Producto[]>;
  venta$: Observable<Venta[]>;

  idPersonaSeleccionada: number;
  idProductoSeleccionado: number;
  idVentaSeleccionada: number;

  maxFecha: Date =new Date();
  fechaSeleccionada: Date = new Date();

  importe: number;
  venta: number;
  cantidad: number;

  constructor(
    private personaService: PersonaService,
    private productoService: ProductoService,
    private ventaService: VentaService,
    private consultaService: ConsultaService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.persona$ = this.personaService.listar();
    this.producto$ = this.productoService.listar();
    this.venta$ = this.ventaService.listar();
  }

  cambieFecha(e: any){
    console.log(e);
  }

  /*estadoBotonRegistrar(){
    return (this.detalleConsulta.length === 0 || this.idEspecialidadSeleccionada === 0 || this.idMedicoSeleccionado === 0 || this.idPacienteSeleccionado === 0);
  }*/

  aceptar(){
    let persona = new Persona();
    persona.idPersona = this.idPersonaSeleccionada;

    let producto = new Producto();
    producto.idProducto = this.idProductoSeleccionado;

    let venta = new Venta();
    venta.idPersona = persona;

    let detalleVentas = new DetalleVentas();
    detalleVentas.idProducto = producto;
    detalleVentas.idVenta = venta;
    detalleVentas.cantidad = this.cantidad;
    /*let consulta = new Consulta();
    consulta.especialidad = especialidad;
    consulta.medico = medico;
    consulta.paciente = paciente;
    consulta.numConsultorio = "C1";*/
  

    venta.fecha = moment(this.fechaSeleccionada).format('YYYY-MM-DDTHH:mm:ss');
    venta.importe = this.importe;

    let consultaDTO = new ConsultaDTO();
    consultaDTO.venta = venta;
    consultaDTO.detalleVenta = detalleVentas;

    this.consultaService.registrarTransaccion(consultaDTO).subscribe( () => {
      this.snackBar.open("Se Registró", "Aviso", { duration: 2000 });

      setTimeout(() => {
        this.limpiarControles();
      }, 2000);
    });
  }

  limpiarControles(){
    //this.detalleConsulta = [];
    //this.examenesSeleccionados = [];
    this.cantidad = null;
    this.importe = null;
    this.venta = null;
    this.idPersonaSeleccionada = 0;
    this.idProductoSeleccionado = 0;
    this.idVentaSeleccionada = 0;
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
  }  
}
