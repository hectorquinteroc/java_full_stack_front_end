import { switchMap } from 'rxjs/operators';
import { Producto } from './../../../_model/producto';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ProductoService } from './../../../_service/producto.service';

@Component({
  selector: 'app-producto-edicion',
  templateUrl: './producto-edicion.component.html',
  styleUrls: ['./producto-edicion.component.css']
})
export class ProductoEdicionComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router, 
    private productoService: ProductoService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'marca': new FormControl('', Validators.required)
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });
  }

  get f(){ return this.form.controls; }

  private initForm() {
    if (this.edicion) {

      this.productoService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idProducto),
          'nombre': new FormControl(data.nombre),
          'marca': new FormControl(data.marca),
        });

      });
    }
  }

  operar() {
    if (this.form.invalid) { return; }

    let paciente = new Producto();
    paciente.idProducto = this.form.value['id'];
    paciente.nombre = this.form.value['nombre'];
    paciente.marca = this.form.value['marca'];

    if (this.edicion) {
      this.productoService.modificar(paciente).pipe(switchMap (() => {
        return this.productoService.listar();
      })).subscribe(data => {
        this.productoService.setProductoCambio(data);
        this.productoService.setMensajeCambio('SE MODIFICO');
      });
    } else {

      this.productoService.registrar(paciente).subscribe(() => {
        this.productoService.listar().subscribe(data => {
          this.productoService.setProductoCambio(data);
          this.productoService.setMensajeCambio('SE REGISTRO');
        });
      });
    }

    this.router.navigate(['producto']);
  }
}
